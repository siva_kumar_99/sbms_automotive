:%s/\r$//
:x
##########
#DESCRIPTION: Script to launch existing 5 jobs concerantly and get the PID for the scripts
#WRITTEN: Siva Kumar
#CREATED_DATE: 8-3-2021
#SCRIPT: /bin/sh /home/azureuser/sbms/lead_acid/automotive/MONITOR/SCRIPTS/Monitoring_script.sh 1>/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS/Monitoring_script.txt 2>&1
#UPDATED DATE/PERSON/REASON:
##################################
##################################

#Script_path="${Log_path}"
Log_path="/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS"
Email="/home/azureuser/sbms/lead_acid/automotive/MONITOR/EMAIL"

script_1="stage1_ETL_job"
script_2="cloud_timestamp_processor"
script_3="StartBMS_App"
script_4="StartBMS_Descriptives"
script_5="StartBMS_Crank_Descriptives"
script_6="test_script"

echo "shell script started at:"
p_RunDate=`date +"%Y-%m-%d %H:%M:%S"`
echo "RunDate is ${p_RunDate}"

##########email_variable####################
To="sivakumar.ampojwalam@dristia.com"
E_From="rakesh.cubic@outlook.com"	


#####################to get the PID#########
script_1_pid=`ps -eaf|grep -m1 "[s]tage1_ETL_job"`
script_2_pid=`ps -eaf|grep -m1 "[c]loud_timestamp_processor"`
script_3_pid=`ps -eaf|grep -m1 "[S]tartBMS_App"`
script_4_pid=`ps -eaf|grep -m1 "[S]tartBMS_Descriptives"`
script_5_pid=`ps -eaf|grep -m1 "[S]tartBMS_Crank_Descriptives"`
script_6_pid=`ps -eaf|grep -m1 "[t]est_script"`
unsuccessful_run=0
##################### PID into a temp_file#########
echo "${script_1_pid}" > ${Log_path}/pid_1.txt
echo "${script_2_pid}" > ${Log_path}/pid_2.txt 
echo "${script_3_pid}" > ${Log_path}/pid_3.txt 
echo "${script_4_pid}" > ${Log_path}/pid_4.txt 
echo "${script_5_pid}" > ${Log_path}/pid_5.txt 
echo "${script_6_pid}" > ${Log_path}/pid_6.txt

##################### check script running status and send_email #########

cut -b 10-16 ${Log_path}/pid_1.txt > ${Log_path}/${script_1}_pid.txt
sed -i -e 's/^/kill /' ${Log_path}/${script_1}_pid.txt
stage1_ETL_job_word_count=`wc -w < ${Log_path}/${script_1}_pid.txt`
echo "$stage1_ETL_job_word_count -stage1_ETL_job_word_count"

if [ $stage1_ETL_job_word_count -eq 2 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "stage1_ETL_job_word_count Running"
else

	echo "stage1_ETL_job failed"
	unsuccessful_run=1
	#python3 ${Email}/stage1_ETL_job_email.py
	python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("stage1_ETL_job")'
		
fi

##################### check script_2 running status and send_email #########

cut -b 10-16 ${Log_path}/pid_2.txt > ${Log_path}/${script_2}_pid.txt
sed -i -e 's/^/kill /' ${Log_path}/${script_2}_pid.txt
cloud_timestamp_processor_word_count=`wc -w < ${Log_path}/${script_2}_pid.txt`
echo "${cloud_timestamp_processor_word_count} - cloud_timestamp_processor"


if [ $cloud_timestamp_processor_word_count -eq 2 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "cloud_timestamp_processor_word_count Running"
else

	echo "cloud_timestamp_processor_word_count failed"
	unsuccessful_run=1
	#python3 ${Email}/cloud_timestamp_processor_email.py
	python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("cloud_timestamp")'
	
fi

##################### check script-3 running status and send_email #########
cut -b 10-16 ${Log_path}/pid_3.txt > ${Log_path}/${script_3}_pid.txt
sed -i -e 's/^/kill /' ${Log_path}/${script_3}_pid.txt
StartBMS_App_word_count=`wc -w < ${Log_path}/${script_3}_pid.txt`
echo "${StartBMS_App_word_count} - StartBMS_App"

if [ $StartBMS_App_word_count -eq 2 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "StartBMS_App_word_count Running"
else

	echo "StartBMS_App_word_count failed"
	unsuccessful_run=1
	#python3 ${Email}/StartBMS_Descriptives_email.py
		python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("StartBMS_App")'
	
fi

##################### check script-4 running status and send_email #########
cut -b 10-16 ${Log_path}/pid_4.txt > ${Log_path}/${script_4}_pid.txt
sed -i -e 's/^/kill /' ${Log_path}/${script_4}_pid.txt
StartBMS_Descriptives_word_count=`wc -w < ${Log_path}/${script_4}_pid.txt`
echo "${StartBMS_Descriptives_word_count} - StartBMS_Descriptives"


if [ $StartBMS_Descriptives_word_count -eq 2 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "StartBMS_Descriptives_word_count Running"
else

	echo "StartBMS_Descriptives_word_count failed"
	unsuccessful_run=1
	#python3 ${Email}/StartBMS_Crank_Descriptives_email.py
	python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("StartBMS_Descriptives")'
	
fi

##################### check script-5 running status and send_email #########

cut -b 10-16 ${Log_path}/pid_5.txt > ${Log_path}/${script_5}_pid.txt
sed -i -e 's/^/kill /' ${Log_path}/${script_5}_pid.txt
StartBMS_Crank_Descriptives_word_count=`wc -w < ${Log_path}/${script_5}_pid.txt`
echo "${StartBMS_Crank_Descriptives_word_count} - StartBMS_Crank_Descriptives"


if [ $StartBMS_Crank_Descriptives_word_count -eq 2 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "StartBMS_Crank_Descriptives_word_count Running"
	
else

	echo "StartBMS_Crank_Descriptives_word_count failed"
	unsuccessful_run=1
	#python3 ${Email}/StartBMS_Crank_Descriptives_email.py
	python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("StartBMS_Crank_Descriptives_")'
	
fi

###########TEST_SCRIPT##################### check this running status and send_email #########
cut -b 10-16 ${Log_path}/pid_6.txt > ${Log_path}/${script_6}_pid.txt
sed -i -e 's/^/kill /' ${Log_path}/${script_6}_pid.txt
test_script_word_count=`wc -w < ${Log_path}/${script_6}_pid.txt`
echo "${test_script_word_count} - test_script"

if [ $test_script_word_count -eq 2 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "test_script_word_count Running"
	#unsuccessful_run=1
else

	echo "test_script_word_count failed"
	#test_unsuccessful_run=1
	unsuccessful_run=1
	#python3 ${Email}/test_script_email.py
	python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("test_script")'
		
fi
echo "unsuccessful_run = $unsuccessful_run "
echo "test_unsuccessful_run = $test_unsuccessful_run "

if [ $unsuccessful_run -eq 1 ] 
then
	#java -jar /home/brain/EmailSender/EmailSender.jar ${ls_jobfailed_email_to} "noreply@dristia.com" "test_script failed for RunDate ${p_RunDate}" "Script failed for  Please check log "
	echo "kill-existing process and restart all the scripts"
	#/bin/sh /home/azureuser/sbms/lead_acid/automotive/MONITOR/SCRIPTS/KILL_PID_BEFORE_SCRIPTS_LAUNCH_v1.sh 1>/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS/KILL_PID_BEFORE_SCRIPTS_LAUNCH_v1.txt 2>&1

python3 -c 'import sys;sys.path.insert(0,"/home/azureuser/automotive/Script_Automation_test");import email_module_v2 as m;m.send_email("ignore failure message, re-running all the processes")'
	
	#----------------------re-running all the automotive python scripts-------------------------------
	#/bin/sh /home/azureuser/sbms/lead_acid/automotive/MONITOR/SCRIPTS/sbms_automotive_scrips.sh 1>/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS/sbms_automotive_scrips_${p_RunDate}.txt 2>&1


#	python3 -c 'import email_module; print email_module.send_email("sivakumar.ampojwalam@dristia.com","rakesh.cubic@outlook.com","from module -1")'
#	python3 -c "import email_module; print(email_module.send_email("sivakumar.ampojwalam@dristia.com","rakesh.cubic@outlook.com"," from module -2"))"
#	python3 -c "import email_module;email_module.send_email("sivakumar.ampojwalam@dristia.com","rakesh.cubic@outlook.com"," from module -3")"
	#python3 import email_module_v1;email_module_v1.send_email "sivakumar.ampojwalam@dristia.com" "rakesh.cubic@outlook.com" " from module -4"
	#python3 email_module.py "sivakumar.ampojwalam@dristia.com" "rakesh.cubic@outlook.com" " from module -5"
	#python3 email_module_v1.py "sivakumar.ampojwalam@dristia.com" "rakesh.cubic@outlook.com" " from module -5"
else

	echo "Stripts running in background"
		
fi

##################################
echo "end of script execution"
##echo `date -d 'today' +%Y-%m-%d`
`date +"%Y-%m-%d %H:%M:%S"`

rm ${Log_path}/pid_1.txt
rm ${Log_path}/pid_2.txt
rm ${Log_path}/pid_3.txt
rm ${Log_path}/pid_4.txt
rm ${Log_path}/pid_5.txt
rm ${Log_path}/pid_6.txt

rm ${Log_path}/${script_1}_pid.txt
rm ${Log_path}/${script_2}_pid.txt
rm ${Log_path}/${script_3}_pid.txt
rm ${Log_path}/${script_4}_pid.txt
rm ${Log_path}/${script_5}_pid.txt
rm ${Log_path}/${script_6}_pid.txt
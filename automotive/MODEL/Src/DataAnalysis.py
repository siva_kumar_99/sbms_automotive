import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import logging


from Src.DBConnect import DBConnect

try:
    Obj_DB = DBConnect()
    sourceDB_connection = Obj_DB.getSourceDBconnection()
    destinationDB_connection = Obj_DB.getDestinationDBconnection() 
except Exception as e:
        #sourceDB_connection.close()
        #destinationDB_connection.close()
        print(' production database insertion failed for ',sourceDB_connection)
        logging.exception(' production database insertion failed for ',destinationDB_connection)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)

pro_ids=pd.read_sql('select distinct unique_id from a_module_status_dup',con=sourceDB_connection)
list_unique_ids=pro_ids['unique_id'].to_list()
production_data_dict= {}

for get_id in list_unique_ids:            
    production_data_dict[get_id] = pd.read_sql('select cloud_time,unique_id,live_count,voltage,current_nc,temperature,soc_kf,soh,rul,rul_std from a_module_status_dup where unique_id = '+"'"+str(get_id)+"'",con=destinationDB_connection)        
    
    
DAS_011 = production_data_dict[list_unique_ids[1]] 

DAS_011.columns

cols = ['cloud_time','voltage','current_nc','temperature']
cols2 = ['cloud_time','soc_kf','soh']

df = DAS_011[cols]
df['cloud_time'] = pd.to_datetime(df['cloud_time'])
df.set_index('cloud_time',inplace=True,drop=True)

%matplotlib qt
df.plot()


df = DAS_011[cols2]
df.set_index('cloud_time',inplace=True,drop=True)

%matplotlib qt
df.plot()


fig1, ax1 = plt.subplots()
ax1.set_title('Basic Plot')
ax1.boxplot(df['temperature'])

#ax = sns.boxplot(x="cloud_time",y="voltage",data=df)
#ax = sns.boxplot(x="day", y="total_bill", data=df)
#ax = sns.swarmplot(x="day", y="total_bill", data=tips, color=".25")





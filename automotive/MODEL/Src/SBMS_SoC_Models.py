# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 04:48:56 2020

@author: Zaki.
"""
import pandas as pd

class KFSoCEstimator:
    def __init__(self):
        self.previous_sigmax = 0
        self.previous_soc_kf = 0
        self.init_soc_count = 0
        print('KFSoCEstimator Object Created.')
    ## checks the soc do not exceedthe ranges (0 - 100) #
    def checkSoc(self,current_soc):
        if float(current_soc) > 100:
            soc = 100
        elif( float(current_soc) < 0):
            soc = 0
        else:
            soc = float(current_soc)
        return soc

    def get_soc_cc(self,prevsoc,current,steptime,capacity):
        presentUsage = (float(current)*steptime)/(3600*capacity)
        presentsoc = float(prevsoc) + (presentUsage*100)
        soc = self.checkSoc(presentsoc)
        soc=format(soc,'.2f')
        return soc
    
    def get_soc_ini(self,volt,sourceDB_connection):
        socdata_lookup = pd.read_sql('select soc, ocv_min, ocv_max, temperature from initial_soc_test_env where ocv_max >= '+str(volt)  ,con=sourceDB_connection)
        if volt>12.9:
            soc_ini = 100
        elif volt<11.7:
            soc_ini = 0
        else:
            data = socdata_lookup[socdata_lookup['ocv_max']>=volt]
            soc_diff_perc = (volt - data[-1:]['ocv_min'])/(data[-1:]['ocv_max'] - data[-1:]['ocv_min'])
            soc_ini = int(data[-1:]['soc']) + int(soc_diff_perc*10)
        return soc_ini

    def kalman_filter(self,xhat,SigmaX,voltage,current,StepTime,total_capacity):
        u = current
        condition1 = u<-60
        condition2 = u>0 and voltage < 12.9
        if condition1 or condition2:
            xhat = self.get_soc_cc(xhat,current,StepTime,total_capacity)
        else:
            SigmaW = 10**-5 #Process noise covariance
            SigmaV = 0.025 #Sensor noise covariance
            A = 1
            B = (StepTime/(3600*total_capacity))
            
            xhat=float(xhat)/100
            if u<0: ########### change
                intercept = 10.4455
                slope = 1.9309
            elif u==0:
                intercept = 11.8682
                slope = 0.7909
            else:
                intercept = 12.5427
                slope = 1.8491
            
            ytrue = voltage
            #KF Step 1: State estimate time update
            xhat = (A*xhat) + (B*u) #use prior value of "u"
            #KF Step 2: Error covariance time update
            SigmaX = ((A*SigmaX)*A) + SigmaW
            # KF Step 3: Estimate system output
            yhat = (slope*xhat) + intercept
            #KF Step 4: Compute Kalman gain matrix
            SigmaY = ((slope*SigmaX)*slope) + SigmaV
            L = (SigmaX*slope)/SigmaY
            #KF Step 5: State estimate measurement update
            innovation = ytrue - yhat
            xhat = xhat + (L*innovation)
            #KF Step 6: Error covariance measurement update
            SigmaX = SigmaX - ((L*slope)*SigmaX)
            xhat = self.checkSoc(xhat*100)
        return xhat,SigmaX
    def remove_noise(self,current):
        current=float(current)
        if current <2 and current>-2: 
            curr = 0
        else:
            curr = current
        return curr

    def get_step_time(self,current): 
        condition1 = (abs(current)>100)
        condition2 = ((current >= -100) & (current <= -25))
        condition3 = ((current > -1) & (current <= 0))
        condition4 = ((current > -25) & (current <= -1))
        if condition1:
            step_time = 0.01
        elif condition2:
            step_time = 0.05
        elif condition3:
            step_time = 60
        elif condition4:
            step_time = 60
        else:
            step_time = 60
        return step_time
    
    

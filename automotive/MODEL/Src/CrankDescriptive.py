# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np


class CrankDescriptive:
    
    def __init__(self):
        print('CrankDescriptive Class object Created...')
        self.lastCrank_Livecount = 0
        self.descrip_crank_info= {"Crank_curr": [],"Crank_volt": [],"prev_current": [],"prev_volt": [],"time":[],"live_count":[],"temperature":[],"soh":[],"unique_id":[],"das_id" : []}
        self.prev_curr = 0
        self. prev_volt = 12.6
        self. crank_counter = 0
        
    def get_mode(self,data):
        data.columns = ['unique_id', 'asset_token', 'live_count', 'cloud_time', 'voltage','current', 'temperature', 'soh']
        data['mode'] = 'R'
        
        n = len(data)
        for i in range(n):
            if (data["current"].iloc[i] > -3.5) & (data['voltage'].iloc[i] < 13.3):
                data['mode'].iloc[i] = 'R'
            elif data.iloc[i]['voltage'] >= 13.3:
                data['mode'].iloc[i] = 'D'
            elif data['current'].iloc[i] < -60:
                data['mode'].iloc[i] = 'C'
    
        for i in range(1,n-1):
            if data.iloc[i]['mode'] == 'R' and data.iloc[i-1]['mode'] == 'C' and data.iloc[i+1]['mode'] == 'D':
                data['mode'].iloc[i] = 'D'
        return data
    
    def get_crank_info(self,descrip_crank_info):
        crank_curr = min(descrip_crank_info["Crank_curr"])
        crank_volt = min(descrip_crank_info["Crank_volt"])
        crank_time = descrip_crank_info["time"][0]
        #das_id = crank_data['das_id'][0]
        live_count = descrip_crank_info['live_count'][0]
        soh = descrip_crank_info['soh'][0]
        prev_volt = descrip_crank_info["prev_volt"][0]
        prev_current = descrip_crank_info["prev_current"][0]
        temperature = np.mean(descrip_crank_info["temperature"])
        unique_id = descrip_crank_info["unique_id"][0]
        asset_token = descrip_crank_info["das_id"][0]
        vdiff = prev_volt-crank_volt
        crank_info = dict(unique_id =unique_id, asset_token =asset_token, live_count=live_count, current =crank_curr,voltage = crank_volt ,voltage_drop = vdiff,prev_volt =prev_volt,prev_current = prev_current, timestamp = crank_time,temperature = temperature,soh=soh)
        return crank_info
    
    def get_time_btw_cranks(self,data):
        data.sort_values(by = ['live_count'],inplace = True)
        time_bw_cranks = 0
        for i in range(len(data)):
            if (data['mode'].iloc[i] == 'R') or (data['mode'].iloc[i] == 'D'):
                time_bw_cranks +=1
        return time_bw_cranks
    
    def get_engine_rest_time(self,data):
        data.sort_values(by = ['live_count'],inplace = True)
        engine_rest_time = 0
        data_rest = data[data['mode'] == 'R']
        #print(data)
        current_mean = np.mean(np.array(data_rest["current"]))
        current_std = np.std(np.array(data_rest["current"]))
        voltage_mean = np.mean(np.array(data_rest["voltage"]))
        voltage_std = np.std(np.array(data_rest["voltage"]))
        for i in range(len(data)):
            if (data['mode'].iloc[i] == 'R'):
                engine_rest_time +=1
        return engine_rest_time,current_mean,current_std,voltage_mean,voltage_std
    
    def make_columns(self,mode_data,crank_info,counter):
        data_main = mode_data
        data = pd.Series(crank_info)
        
        # Time Column
        data['timestamp'] = pd.to_datetime(data['timestamp'])
        #print(data['timestamp'])
        data['Time'] = data['timestamp'].time()
        
        # Make Date Column
        data['Date'] = data['timestamp'].date()
        
        # Make Hour column
        data['Hour'] = int(data['timestamp'].hour)
        
        # Make Month Column
        data['Month'] = data['timestamp'].month
        
        # Exp Voltage Column
        data['exp_voltage'] = np.exp(data['voltage'])
        
        # Make Exp Voltage Drop Column
        data['voltage_drop_exp'] = np.exp(data['voltage_drop'])
        
    
        data['temperature_label'] = ('Less Than 18' if data.loc['temperature']<18 
                                                 else '18-20' if data.loc['temperature']<=20
                                                 else '20-22' if data.loc['temperature']<=22
                                                 else '22-24' if data.loc['temperature']<=24
                                                 else '24-26' if data.loc['temperature']<=26
                                                 else '26-28' if data.loc['temperature']<=28
                                                 else '28-30' if data.loc['temperature']<=30
                                                 else '30-32' if data.loc['temperature']<=32
                                                 else '32-34' if data.loc['temperature']<=34
                                                 else '34-36' if data.loc['temperature']<=36
                                                 else '36-38' if data.loc['temperature']<=38
                                                 else '38-40' if data.loc['temperature']<=40
                                                 else '40-42' if data.loc['temperature']<=42
                                                 else 'Greater Than 42')
    
    
        data['time_bins'] = ( 'Midnight - 4 am' if data.loc['Hour']<=4
                                                 else '4 am-8 am' if data.loc['Hour']<=8
                                                 else '8 am- Noon' if data.loc['Hour']<=12
                                                 else 'Noon-4 pm' if data.loc['Hour']<=16
                                                 else '4 pm-8 pm' if data.loc['Hour']<=20
                                                 else '8 pm-Midnight')
        
     
        data['IR'] = -1*(data['voltage_drop']/data['current'])
        data['time_btw_cranks'] = self.get_time_btw_cranks(data_main)
        data['engine_rest_time'],data['current_mean'],data['current_std'],data['voltage_mean'],data['voltage_std'] = self.get_engine_rest_time(data_main)
        data['time_bw_cranks_bin'] = ('Less than 4H' if data.loc['engine_rest_time']<240
                                                 else '4H-8H' if data.loc['engine_rest_time']<=480
                                                 else '8H-12H' if data.loc['engine_rest_time']<=720
                                                 else '12H-16H' if data.loc['engine_rest_time']<=960
                                                 else '16H-20H' if data.loc['engine_rest_time']<=1200
                                                 else '20H-24H' if data.loc['engine_rest_time']<=1440
                                                 else 'Greater than 24H')
    
        data['crank_index'] = counter
        
        
        return data

    def get_crankData(self,live_count,get_id,current,Voltage,prev_volt,prev_curr,time,temperature,soh,unique_id,das_id,descrip_crank_info,destinationDB_connection):
        if(current > -80):
            if(len(descrip_crank_info["Crank_curr"]) > 3):
                sql_stmt = 'SELECT unique_id,asset_token,live_count,cloud_time,voltage,current_nc,temperature,soh FROM sbms_stg.a_module_status_dup where unique_id='+"'"+str(get_id)+"'"+'and live_count between'+"'"+str(self.lastCrank_Livecount)+"'"+'and'+"'"+str(live_count-1)+"'"+';'
                
                #print("live_count    -----------",live_count)
                pro_crankData_df = pd.read_sql(sql_stmt,con=destinationDB_connection)
                pro_crankData_df.drop_duplicates(subset ="live_count", keep ='first' , inplace = True)
                pro_crankData_df.sort_values(by=['live_count'], ascending=True,inplace = True)
                pro_crankData_df.reset_index(drop=True,inplace = True)
                crank_info = self.get_crank_info(descrip_crank_info)
                self. crank_counter += 1
                pro_crankData_df = self.get_mode(pro_crankData_df)
                crank_desp_df = self.make_columns(pro_crankData_df,crank_info,self. crank_counter)
                crank_desp_df.replace(r'\s+', np.nan, regex=True)
                
                crank_desp_df =  (crank_desp_df.to_frame()).T
                crank_desp_df.dropna(inplace = True)
                self.lastCrank_Livecount = live_count
                
                descrip_crank_info = self.clear_descrip_crank_data(descrip_crank_info)
                
                #crank_desp_df.to_csv("crank_desp_df.csv")
                return crank_desp_df
        else:
            descrip_crank_info = self.append_descrip_crank_data(descrip_crank_info,current,Voltage,prev_curr,prev_volt,time,live_count,temperature,soh,unique_id,das_id)
            return None  
    
    def clear_descrip_crank_data(self,descrip_crank_info):
        descrip_crank_info["Crank_curr"] = []
        descrip_crank_info["Crank_volt"] = []
        descrip_crank_info["prev_current"] = []
        descrip_crank_info["prev_volt"] = []
        descrip_crank_info["time"] = []
        descrip_crank_info["live_count"] = []
        descrip_crank_info["temperature"] = []
        descrip_crank_info["soh"] = []
        descrip_crank_info["unique_id"] = []
        descrip_crank_info["das_id"] = []
        return descrip_crank_info
    def append_descrip_crank_data(self,descrip_crank_info,current,voltage,prev_curr,prev_voltage,time,live_count,temperature,soh,unique_id,das_id):
        descrip_crank_info["Crank_curr"].append(current)
        descrip_crank_info["Crank_volt"].append(voltage)
        descrip_crank_info["prev_current"].append(prev_curr)
        descrip_crank_info["prev_volt"].append(prev_voltage)
        descrip_crank_info["time"].append(time)
        descrip_crank_info["live_count"].append(live_count)
        descrip_crank_info["temperature"].append(temperature)
        descrip_crank_info["soh"].append(soh)
        descrip_crank_info["unique_id"].append(unique_id)
        descrip_crank_info["das_id"].append(das_id)
        return descrip_crank_info
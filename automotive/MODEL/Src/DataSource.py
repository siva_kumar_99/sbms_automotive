

from Src.DBConnect import DBConnect
import logging
import pandas as pd


try:
    Obj_DB = DBConnect()
    sourceDB_connection = Obj_DB.getSourceDBconnection()
    destinationDB_connection = Obj_DB.getDestinationDBconnection() 
    
except Exception as e:
        print(' production database insertion failed for ',sourceDB_connection)
        logging.exception(' production database insertion failed for ',destinationDB_connection)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)


print(sourceDB_connection)
print(destinationDB_connection)

pro_ids=pd.read_sql('select distinct unique_id from module_rawdata',con=sourceDB_connection)
list_unique_ids=pro_ids['unique_id'].to_list()


pro_ids=pd.read_sql('select distinct unique_id from module_rawdata',con=sourceDB_connection)

UID_lst = ['5fce4a6884a4a13ab34e2114', '5fe30200183c093b58bc33ff', '5fec6195183c093b58c0a1ae',
 '5fed87e4183c093b58c1794e','5ff4565e183c093b58c3acf1', '5ff58f80183c093b58c440ef', '5ff6cb5f183c093b58c4cc52',
 '5ffd832fe146ca4044921aa7', '5fcee78e84a4a13ab34e2640', '60225afac2e4803f50207b11']

for uid in ['5fec6195183c093b58c0a1ae']:
    production_df = pd.read_sql('select unique_id,live_count,cloud_time,voltage,current_nc,temperature from a_module_status_dup where unique_id = '+"'"+str(uid)+"' order by live_count desc limit 1",con=destinationDB_connection)


"""
Created on Thu Dec  16 03:05:56 2020

@author: Hari_Ramaraju.

@version : V1.0
"""

import pandas as pd
import logging
import time

from Src.DBConnect import DBConnect
from Src.CrankDescriptive import CrankDescriptive

try:
    Obj_DB = DBConnect()
    sourceDB_connection = Obj_DB.getSourceDBconnection()
    destinationDB_connection = Obj_DB.getDestinationDBconnection() 
    
except Exception as e:
    
        print(' production database insertion failed for ',sourceDB_connection)
        logging.exception(' production database insertion failed for ',destinationDB_connection)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)
    

#Objects Functional Area
def createCrankDesObjects(UID_list,Objs_dict):
    for id in UID_list:
        if id not in Objs_dict:
            Obj_Crank_Descrip = CrankDescriptive()
            Objs_dict[id] = {"Obj_Crank_Descrip":Obj_Crank_Descrip}
    return Objs_dict


def pushRecordTOCrankDescriptiveTable(descriptive_push_df,destinationDB_connection):
    try:
        descriptive_push_df.to_sql(con=destinationDB_connection, name='crank_descriptives_table',if_exists='append',index=False)
    except Exception as e:
        print('production database insertion failed for ', get_id)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)

    
    
    
    
    
def getMinMaxLiveCount(df):
    min_livecount = min(df['live_count'])
    max_livecount = max(df['live_count'])

    return min_livecount,max_livecount



Objs_Crank_dict= {} 

while True:
    #print('Batch Process Started...')
    device_UIDs =pd.read_sql('select distinct unique_id from a_module_status_dup',con=sourceDB_connection)
    device_UIDs_lst = device_UIDs['unique_id'].to_list()
    
    Objs_Crank_dict = createCrankDesObjects(device_UIDs_lst,Objs_Crank_dict)

    for get_id in device_UIDs_lst: 
        production_df = pd.read_sql('select unique_id,asset_token,live_count,cloud_time,voltage,current_nc,temperature,soh,descriptive_crank_flag from a_module_status_dup where unique_id = '+"'"+str(get_id)+"'"+" and descriptive_crank_flag = "+"'"+str(0)+"'",con=destinationDB_connection)
        #timestamp_df = pd.read_sql('select live_count,cloud_time from timestamp_table where unique_id = "{0}" and live_count <= {1}'.format(get_id,max(production_df1['live_count'])),con=destinationDB_connection)
        #production_df = pd.merge(production_df1,timestamp_df,on='live_count',how='inner')
        #crakn_descriptive = pd.read_sql('select unique_id,date,live_count from descriptive_table where unique_id = '+"'"+str(get_id)+"'"+" order by live_count desc limit 1 ",con=destinationDB_connection)
        production_df.drop_duplicates(subset ="live_count", keep ='first' , inplace = True)
        production_df.sort_values(by=['live_count'], ascending=True,inplace = True)
        production_df.reset_index(drop=True,inplace = True)
        #if(len(production_df) == 0):
            #print("No New Record for id: ",get_id)
        for i in range(len(production_df)):
            voltage=production_df['voltage'][i]
            current=production_df['current_nc'][i]
            temperature=production_df['temperature'][i]
            live_count=production_df['live_count'][i]
            unique_id=production_df['unique_id'][0]
            soh=production_df['soh'][i]
            assettoken=production_df['asset_token'][0]
            #mobile_time=str(production_df['mobile_time'][i])
            cloud_time=str(production_df['cloud_time'][i])
            
            
            crank_desciptive_df = Objs_Crank_dict[get_id]['Obj_Crank_Descrip'].get_crankData(live_count,get_id,current,voltage,Objs_Crank_dict[get_id]['Obj_Crank_Descrip'].prev_volt,Objs_Crank_dict[get_id]['Obj_Crank_Descrip'].prev_curr,cloud_time,temperature,soh,get_id,assettoken,Objs_Crank_dict[get_id]['Obj_Crank_Descrip'].descrip_crank_info,destinationDB_connection)
            Objs_Crank_dict[get_id]['Obj_Crank_Descrip'].prev_volt = voltage
            Objs_Crank_dict[get_id]['Obj_Crank_Descrip'].prev_curr = current
            if((crank_desciptive_df is not None) and (len(crank_desciptive_df) > 0)):
                push_crank_desciptive_df = pd.DataFrame({'unique_id':crank_desciptive_df.unique_id.iloc[0],'asset_token':crank_desciptive_df.asset_token.iloc[0], 'live_count':crank_desciptive_df.live_count.iloc[0], 'current':crank_desciptive_df.current.iloc[0], 'voltage':crank_desciptive_df.voltage.iloc[0],'voltage_drop':crank_desciptive_df.voltage_drop.iloc[0],'prev_volt':crank_desciptive_df.prev_volt.iloc[0],'prev_current':crank_desciptive_df.prev_current.iloc[0], 'timestamp':crank_desciptive_df.timestamp.iloc[0], 'temperature':crank_desciptive_df.temperature.iloc[0],'soh':crank_desciptive_df.soh.iloc[0],'Time':crank_desciptive_df.Time.iloc[0], 'Date':crank_desciptive_df.Date.iloc[0],'Hour':crank_desciptive_df.Hour.iloc[0], 'Month':crank_desciptive_df.Month.iloc[0], 'exp_voltage':crank_desciptive_df.exp_voltage.iloc[0],'voltage_drop_exp':crank_desciptive_df.voltage_drop_exp.iloc[0], 'temperature_label':crank_desciptive_df.temperature_label.iloc[0], 'time_bins':crank_desciptive_df.time_bins.iloc[0],'IR':crank_desciptive_df.IR.iloc[0],'time_btw_cranks':crank_desciptive_df.time_btw_cranks.iloc[0], 'engine_rest_time':crank_desciptive_df.engine_rest_time.iloc[0], 'current_mean':crank_desciptive_df.current_mean.iloc[0], 'current_std':crank_desciptive_df.current_std.iloc[0],'voltage_mean':crank_desciptive_df.voltage_mean.iloc[0], 'voltage_std':crank_desciptive_df.voltage_std.iloc[0], 'time_bw_cranks_bin':crank_desciptive_df.time_bw_cranks_bin.iloc[0], 'crank_index':crank_desciptive_df.crank_index.iloc[0]},index=[0])
                pushRecordTOCrankDescriptiveTable(push_crank_desciptive_df,destinationDB_connection)
                #print("crank_desciptive_df",crank_desciptive_df)
                #crank_desciptive_df.to_csv("crank_df.csv")
            
        #print("Processed ",get_id,"till live Count: ",live_count)
        
        
        if (len(production_df) > 0):
                min_livecount,max_livecount = getMinMaxLiveCount(production_df)
                if max_livecount != 0:
                    print('MinMax_LiveCount',min_livecount,':',max_livecount)
                    run_sql_stmt = "update a_module_status_dup set descriptive_crank_flag ="+"'"+str(1)+"'"+" where "+'('+"unique_id = "+"'"+str(unique_id)+"'"+" and "+'('+"live_count between "+"'"+str(min_livecount)+"'"+" and "+"'"+str(max_livecount)+"'"+'));'
                    destinationDB_connection.execute(run_sql_stmt)

    time.sleep(900)